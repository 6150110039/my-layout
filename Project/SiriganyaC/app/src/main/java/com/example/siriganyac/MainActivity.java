package com.example.siriganyac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.net.URL;
import java.security.PrivateKey;

public class MainActivity extends AppCompatActivity {
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent categoryIntent = new Intent(MainActivity.this, SendEmail.class);
                startActivity(categoryIntent);
                finish();
            }
        });

        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent  = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com/siriganyac"));
                startActivity(browserIntent);
                finish();
            }
        });

        button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent  = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/?gl=TH"));
                startActivity(browserIntent);
                finish();
            }
        });

        button4 = findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent  = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.spotify.com/th-th/"));
                startActivity(browserIntent);
                finish();
            }
        });

        button5 = findViewById(R.id.button5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent categoryIntent = new Intent(MainActivity.this, AboutMe.class);
                startActivity(categoryIntent);
                finish();
            }
        });








    }

    }
